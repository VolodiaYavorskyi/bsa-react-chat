import { uuidv4 } from './uuid';

export const newMessage = (inputText) => ({
    id: uuidv4(),
    userId: "logged_user",
    avatar: '',
    user: "logged_user",
    text: inputText.trim(),
    createdAt: new Date().toISOString(),
    editedAt: ''
});

export const addNewMessage = (messages, message, inputText) => {
    if (inputText.trim() === '') {
        return messages;
    }
    return [...messages, message];
}