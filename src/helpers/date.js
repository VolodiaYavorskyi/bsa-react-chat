function toHoursAndMinutes(dateInput) {
    const date = new Date(dateInput);
    return date.getHours() + ':' + date.getMinutes();
}

function toDisplayDate(dateInput) {
    const date = new Date(dateInput);
    const now = new Date(Date.now());
    const yesterday = new Date(new Date(now).setDate(now.getDate() - 1));

    if (date.getDate() === now.getDate()
        && date.getMonth() === now.getMonth()
        && date.getFullYear() === now.getFullYear()) {
        return "Today";
    } else if (date.getDate() === yesterday.getDate()
        && date.getMonth() === yesterday.getMonth()
        && date.getFullYear() === yesterday.getFullYear()) {
        return "Yesterday";
    } else {
        const options = { weekday: "long", year: "numeric", month: "long", day: "numeric" };
        return new Intl.DateTimeFormat("en-US", options).format(date);
    }
}

export { toHoursAndMinutes, toDisplayDate };