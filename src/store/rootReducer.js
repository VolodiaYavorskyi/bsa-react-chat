import { REQUEST_MESSAGES, RECEIVE_MESSAGES, CHANGE_INPUT, SEND_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from './actions';
import { addNewMessage } from '../helpers/message';
import { createReducer } from '@reduxjs/toolkit';

const rootReducer = createReducer({
    isLoading: false,
    messages: [],
    inputText: ''
}, {
    [REQUEST_MESSAGES]: (state, action) => { state.isLoading = true; },
    [RECEIVE_MESSAGES]: (state, action) => { state.isLoading = false; state.messages = action.payload.messages },
    [CHANGE_INPUT]: (state, action) => { state.inputText = action.payload },
    [SEND_MESSAGE]: (state, action) => { state.messages = addNewMessage(state.messages, action.payload.message, state.inputText); state.inputText = '' },
    [EDIT_MESSAGE]: (state, action) => {
        state.messages = state.messages.map(msg => msg.id === action.payload.message.id ?
            { ...msg, text: action.payload.newText, editedAt: new Date().toISOString() } : msg)
    },
    [DELETE_MESSAGE]: (state, action) => { state.messages = state.messages.filter(msg => msg.id !== action.payload.id) }
})

export default rootReducer;