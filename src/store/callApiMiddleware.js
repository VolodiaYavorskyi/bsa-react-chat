const callApi = (_store) => (next) => (action) => {
    const { type, payload, ...rest } = action;
    if (!payload)
        return next(action);
    const { callApi } = payload;
    if (!callApi)
        return next(action);

    fetch(callApi.url, callApi.params)
        .then((res) => res.json())
        .then((data) => {
            next({
                ...rest,
                type,
                payload: {
                    ...payload,
                    response: data
                }
            });
        })
        .catch((err) => {
            next({
                ...rest,
                type,
                payload: {
                    error: err
                }
            });
        });
};

export { callApi };