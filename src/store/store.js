import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './rootReducer';
import { callApi } from './callApiMiddleware';

const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(callApi)
});

export default store;