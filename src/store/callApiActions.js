export const receiveMessagesCallApi = messages => ({
    url: 'http://localhost:3001/messages',
    params: { method: 'PUT', body: JSON.stringify(messages), headers: { 'Content-Type': 'application/json' } }
});

export const deleteMessageCallApi = id => ({
    url: 'http://localhost:3001/messages/' + id,
    params: { method: 'DELETE' }
});

export const sendMessageCallApi = message => ({
    url: 'http://localhost:3001/messages',
    params: { method: 'POST', body: JSON.stringify(message), headers: { 'Content-Type': 'application/json' } }
});

export const editMessageCallApi = (message, newText) => ({
    url: 'http://localhost:3001/messages/' + message.id,
    params: { method: 'PUT', body: JSON.stringify({ ...message, text: newText, editedAt: new Date().toISOString() }), headers: { 'Content-Type': 'application/json' } }
});