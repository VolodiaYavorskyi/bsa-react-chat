import { createAction } from '@reduxjs/toolkit';
import { receiveMessagesCallApi, deleteMessageCallApi, sendMessageCallApi, editMessageCallApi } from './callApiActions';
import { loadJsonData } from '../helpers/loader';

export const REQUEST_MESSAGES = createAction('REQUEST_MESSAGES');
export const RECEIVE_MESSAGES = createAction('RECEIVE_MESSAGES');

export const CHANGE_INPUT = createAction('CHANGE_INPUT');
export const SEND_MESSAGE = createAction('SEND_MESSAGE');
export const EDIT_MESSAGE = createAction('EDIT_MESSAGE');
export const DELETE_MESSAGE = createAction('DELETE_MESSAGE');

const requestMessages = url => REQUEST_MESSAGES({ url });
const receiveMessages = (url, json) => RECEIVE_MESSAGES({ url, messages: json });
const receiveMessagesWithPut = (url, json) => RECEIVE_MESSAGES({ url, messages: json, callApi: receiveMessagesCallApi(json) });

export const fetchMessages = url => async (dispatch) => {
    dispatch(requestMessages(url));
    const dbaseJson = await loadJsonData('http://localhost:3001/messages');
    if (Array.isArray(dbaseJson)) {
        return dispatch(receiveMessages(url, dbaseJson));
    }
    const json = await loadJsonData(url);
    return dispatch(receiveMessagesWithPut(url, json));
}

export const changeInput = inputText => CHANGE_INPUT(inputText);
export const sendMessage = (message) => SEND_MESSAGE({ message, callApi: sendMessageCallApi(message) });
export const editMessage = (message, newText) => EDIT_MESSAGE({ message, newText, callApi: editMessageCallApi(message, newText) });
export const deleteMessage = id => DELETE_MESSAGE({ id, callApi: deleteMessageCallApi(id) });