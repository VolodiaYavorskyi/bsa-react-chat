import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchMessages } from '../../store/actions';
import ChatContent from './ChatContent';
import EditorPage from '../message/EditorPage';
import logo from '../../images/logo.png';
import './Chat.css';

class Chat extends React.Component {
    static propTypes = {
        messages: PropTypes.array.isRequired,
        isLoading: PropTypes.bool.isRequired,
        url: PropTypes.string.isRequired,
        dispatch: PropTypes.func.isRequired
    }

    componentDidMount() {
        const { dispatch, url } = this.props;
        dispatch(fetchMessages(url));
    }

    render() {
        const { messages, isLoading } = this.props;

        return (
            <div className="chat">
                <Link className="chat-logo-link" to="/">
                    <div className="logo">
                        <img src={logo} alt="logo" />
                        <div className="logo-name">BSA Chat</div>
                    </div>
                </Link>
                <Switch>
                    <Route exact path="/">
                        <ChatContent messages={messages} isLoading={isLoading} />
                    </Route>
                    <Route path="/edit/:id">
                        <EditorPage messages={messages} />
                    </Route>
                </Switch>
                <footer>
                    <div className="footer-text">Copyright</div>
                </footer>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const { messages, isLoading } = state;

    return { messages, isLoading };
}

export default connect(mapStateToProps)(Chat);