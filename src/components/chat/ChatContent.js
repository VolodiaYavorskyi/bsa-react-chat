import Preloader from '../preloader/Preloader';
import Header from '../header/Header';
import MessageList from '../message/MessageList';
import MessageInput from '../message_input/MessageInput';

function ChatContent(props) {
    const { messages, isLoading } = props;

    return (
        <div className="chat-content">
            <Preloader visible={isLoading} />
            <Header messages={messages} />
            <MessageList messages={messages} />
            <MessageInput />
        </div>
    );
}

export default ChatContent;