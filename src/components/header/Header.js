import { toHoursAndMinutes } from '../../helpers/date';
import './Header.css';

function Header({ messages }) {
    const headerTitle = "My chat";
    const userCount = "23";
    const messagesCount = messages.length;
    const lastMessageDate = messagesCount > 0
        ? "last message at " + toHoursAndMinutes(messages[messagesCount - 1].createdAt)
        : "no messages";

    return (
        <div className="header">
            <div className="left-part">
                <div className="header-title">{headerTitle}</div>
                <div className="header-users-count">{userCount} participants</div>
                <div className="header-messages-count">{messagesCount} messages</div>
            </div>
            <div className="right-part">
                <div className="header-last-message-date">{lastMessageDate}</div>
            </div>
        </div>
    );
}

export default Header;