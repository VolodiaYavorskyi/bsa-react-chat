import Message from './Message';
import OwnMessage from './OwnMessage';
import { toDisplayDate } from '../../helpers/date';
import './Message.css';

function MessageList({ messages }) {
    let prevDate;
    const mappedMessages = messages.map(msg => {
        const msgDate = toDisplayDate(msg.createdAt);
        const divider = msgDate === prevDate ? null : msgDate;
        prevDate = msgDate;

        if (msg.userId === "logged_user") {
            return <OwnMessage key={msg.id} message={msg} divider={divider} />
        } else {
            return <Message key={msg.id} message={msg} divider={divider} />
        }
    })

    return (
        <div className="message-list">
            {mappedMessages}
        </div>
    );
}

export default MessageList;