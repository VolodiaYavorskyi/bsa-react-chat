import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import GeneralMessage from './GeneralMessage';
import { deleteMessage } from '../../store/actions';
import { toHoursAndMinutes } from '../../helpers/date';
import './Message.css';

function OwnMessage({ dispatch, divider, message }) {
    const { text } = message;
    const time = toHoursAndMinutes(message.createdAt);

    return (
        <GeneralMessage
            divider={divider}
            content={
                <div className="own-message" >
                    <div className="own-message-top-part">
                        <div className="message-text">{text}</div>
                        <div className="message-time">{time}</div>
                    </div>
                    <div className="own-message-bottom-part">
                        <div className="own-message-controls">
                            <div className="own-message-buttons">
                                <div className="own-message-edit">
                                    <Link className="own-message-edit-link" to={"/edit/" + message.id}>
                                        <button><span>&#9998;</span></button>
                                    </Link>
                                </div>
                                <div className="own-message-delete">
                                    <button onClick={() => dispatch(deleteMessage(message.id))}><span>&#9249;</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        />
    );
}

export default connect()(OwnMessage);