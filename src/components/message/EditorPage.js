import { useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { editMessage } from '../../store/actions';
import './Message.css';

function EditorPage({ dispatch, messages }) {
    const { id } = useParams();
    const history = useHistory();
    const [input, setInput] = useState('');

    const message = messages.find(msg => msg.id === id);

    return (
        <div className="editor-page">
            <div className="editor-header">Edit message</div>
            <form className="editor-input"
                onSubmit={(e) => {
                    e.preventDefault();
                    dispatch(editMessage(message, input));
                    setInput('');
                    history.push("/");
                }}>
                <textarea className="editor-input-text"
                    onChange={e => setInput(e.target.value)} value={input}
                />
                <div className="editor-buttons">
                    <button className="editor-submit-button" type="submit"><span>&#10003;</span></button>
                    <button className="editor-cancel-button" type="cancel"
                        onClick={e => {
                            e.preventDefault();
                            setInput('');
                            history.push("/");
                        }}>X</button>
                </div>
            </form>
        </div>
    );
}

export default connect()(EditorPage);