import './Message.css';

function Divider({ divider }) {
    if (!divider) {
        return null;
    }

    return (
        <div className="messages-divider">
            <div className="messages-divider-text">{divider}</div>
        </div>
    );
}


function GeneralMessage({ divider, content }) {
    return (
        <div className="message-box">
            <Divider divider={divider} />
            <div className="message-content-box">{content}</div>
        </div>
    )
}

export default GeneralMessage;