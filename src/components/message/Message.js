import { useState } from 'react';
import GeneralMessage from './GeneralMessage';
import { toHoursAndMinutes } from '../../helpers/date';
import './Message.css';

function Message({ divider, message }) {
    const [like, setLike] = useState(false);

    const { avatar, userName, text } = message;
    const time = toHoursAndMinutes(message.createdAt);

    return (
        <GeneralMessage
            divider={divider}
            content={
                <div className="message">
                    <div className="message-left-part">
                        <div className="message-user-avatar"><img className="user-avatar" src={avatar} alt="avatar" /></div>
                    </div>
                    <div className="message-right-part">
                        <div className="message-top-part">
                            <div className="message-user-name">{userName}</div>
                        </div>
                        <div className="message-middle-part">
                            <div className="message-text">{text}</div>
                            <div className="message-time">{time}</div>
                        </div>
                        <div className="message-bottom-part">
                            <div className="message-like">
                                <button onClick={() => setLike(!like)}>
                                    {like ? <span>&#9829;</span> : <span>&#9825;</span>}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            }
        />
    );
}

export default Message;