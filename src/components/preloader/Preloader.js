import logo from '../../logo.svg';
import './Preloader.css';

function Preloader({ visible }) {
    if (!visible)
        return null;

    return (
        <div className="preloader">
            <div className="preloader-content">
                <img src={logo} className="preloader-logo" alt="preloader" />
            </div>
        </div>
    );
}

export default Preloader;