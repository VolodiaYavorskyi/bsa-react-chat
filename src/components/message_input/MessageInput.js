import React from 'react';
import { connect } from 'react-redux';
import { changeInput, sendMessage } from '../../store/actions';
import { newMessage } from '../../helpers/message';
import './MessageInput.css';

function MessageInput(props) {
    const { inputText } = props;

    return (
        <form className="message-input" onSubmit={(e) => {
            e.preventDefault();
            props.dispatch(sendMessage(newMessage(inputText)));
        }}>
            <textarea className="message-input-text" placeholder="Message"
                onChange={(e) => props.dispatch(changeInput(e.target.value))} value={inputText} />
            <button className="message-submit-button" type="submit"><span>&#9993;</span></button>
        </form>
    );
}

const mapStateToProps = state => {
    const { inputText } = state;

    return { inputText };
}

export default connect(mapStateToProps)(MessageInput);